package shuja.com.bottlerockettest.network;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.squareup.okhttp.ResponseBody;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import shuja.com.bottlerockettest.controller.MainActivity;

public class DownloadService extends IntentService {

    private String TAG = "Download Service";

    public DownloadService() {
        super("Download Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        initDownload();
    }

    /**
     * This function uses the retrofit library to download the json file from the web endpoint
     **/
    private void initDownload() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://sandbox.bottlerocketapps.com/")
                .build();

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);

        Call<ResponseBody> request = retrofitInterface.downloadFile();
        request.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onResponse(final Response<ResponseBody> response, Retrofit retrofit) {
                if (response.isSuccess()) {

                    new AsyncTask<Void, Long, Void>() {

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                saveFile(response.body());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    }.execute();
                } else {
                    Log.e(TAG, "Download Error");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "Download Error");
            }
        });
    }


    /**
     * This function is used to save the json file into device memory
     **/
    private void saveFile(ResponseBody body) throws IOException {
        int count;
        byte data[] = new byte[1024 * 4];

        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = new File(getApplicationContext().getFilesDir(), "stores.json");
        OutputStream output = new FileOutputStream(outputFile);

        while ((count = bis.read(data)) != -1) {
            output.write(data, 0, count);
        }
        onDownloadComplete();
        output.flush();
        output.close();
        bis.close();
    }


    /**
     * This function is called when the file is downloaded and saved. Contains post download logic like sending notification or
     * sending a broadcast to let activity know download completed.
     **/
    private void onDownloadComplete() {
        Intent intent = new Intent(MainActivity.DOWNLOAD_PROGRESS);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

}
