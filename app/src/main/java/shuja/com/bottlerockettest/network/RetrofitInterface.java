package shuja.com.bottlerockettest.network;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Streaming;

interface RetrofitInterface {
    @GET("BR_Android_CodingExam_2015_Server/stores.json")
    @Streaming
    Call<ResponseBody> downloadFile();
}
