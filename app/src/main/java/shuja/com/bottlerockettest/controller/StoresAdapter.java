package shuja.com.bottlerockettest.controller;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import shuja.com.bottlerockettest.R;
import shuja.com.bottlerockettest.model.StoresEntity;

public class StoresAdapter extends RecyclerView.Adapter<StoresAdapter.ViewHolder> {

    private StoresEntity values;
    private OnItemClicked onItemClicked;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvStoreName;
        public ImageView ivStoreLogo;
        public TextView tvStoreAddress;
        public LinearLayout llStoreItem;
        public TextView tvStorePhone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvStoreName = itemView.findViewById(R.id.tvStoreName);
            ivStoreLogo = itemView.findViewById(R.id.ivStoreLogo);
            tvStoreAddress = itemView.findViewById(R.id.tvStoreAddress);
            tvStorePhone = itemView.findViewById(R.id.tvStorePhone);
            llStoreItem = itemView.findViewById(R.id.llStoreItem);
        }
    }

    public StoresAdapter(StoresEntity storesEntity){
        this.values = storesEntity;
    }

    @NonNull
    @Override
    public StoresAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.store_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoresAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvStoreName.setText(values.getStores().get(i).getName());
        viewHolder.tvStoreAddress.setText(values.getStores().get(i).getAddress());
        viewHolder.tvStorePhone.setText(values.getStores().get(i).getPhone());
        Picasso.get().load(values.getStores().get(i).getStoreLogoURL()).resize(250,250).into(viewHolder.ivStoreLogo);
        viewHolder.llStoreItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClicked.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.getStores().size();
    }

    public void setOnItemClicked(OnItemClicked onItemClicked){
        this.onItemClicked = onItemClicked;
    }
}
