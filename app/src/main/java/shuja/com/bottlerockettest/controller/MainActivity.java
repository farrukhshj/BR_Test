package shuja.com.bottlerockettest.controller;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;

import shuja.com.bottlerockettest.network.DownloadService;
import shuja.com.bottlerockettest.R;
import shuja.com.bottlerockettest.model.StoresEntity;

public class MainActivity extends AppCompatActivity implements OnItemClicked {

    private static final long TIMEOUT_IN_MINUTES = 15;
    public RecyclerView rvStoresList;
    public static String DOWNLOAD_PROGRESS = "download_progress";
    public static String TAG = "MainActivity";
    private Context context;
    private StoresEntity mStoreEntity;
    private LinearLayoutManager mLayoutManager;
    private SharedPreferences mSharedPreferences;
    private long currentTime;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        rvStoresList = findViewById(R.id.rvStoresList);
        mLayoutManager = new LinearLayoutManager(this);
        rvStoresList.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvStoresList.getContext(), mLayoutManager.getOrientation());
        rvStoresList.addItemDecoration(dividerItemDecoration);

        registerBroadcastReceiver();
        checkIfDataAlreadyExists();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void checkIfDataAlreadyExists() {
        if (hasTimeElapsed()) {
            Log.d(TAG, "Either timed out or first run, hence we will download file!");
            if (isNetworkAvailable()) {
                downloadStoreData();
            } else {
                File file = new File(context.getFilesDir() + "/stores.json");
                if (file.exists()) {
                    Log.d(TAG, "I actually timed out but no internet connection, so read from Local file!");
                    readJSONFromFile();
                } else {
                    showNoInternetDialog();
                }
            }
        } else {

            File file = new File(context.getFilesDir() + "/stores.json");
            if (file.exists()) {
                Log.d(TAG, "Data already exist so read from Local file!");
                readJSONFromFile();
            } else {
                Log.d(TAG, "Data does not exist so downloading the file!");
                if (isNetworkAvailable()) {
                    downloadStoreData();
                } else {
                    showNoInternetDialog();
                }
            }
        }
    }

    private void showNoInternetDialog() {
        AlertDialog alertDialogBuilder = new AlertDialog.Builder(this)
                .setTitle("NO INTERNET CONNECTION")
                .setMessage("Why do you think I'll work without Internet?")
                .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
    }

    private boolean hasTimeElapsed() {
        currentTime = Calendar.getInstance().getTimeInMillis();
        String lastWebCallTimeString = mSharedPreferences.getString("LastCallTime", "");

        Log.d(TAG, "Current time=" + currentTime + "\tTime File was downloaded=" + lastWebCallTimeString);

        if (lastWebCallTimeString.isEmpty()) {
            return true;
        } else {
            long diff = currentTime - Long.valueOf(lastWebCallTimeString);
            long diffMinutes = diff / (60 * 1000);
            if (diffMinutes >= TIMEOUT_IN_MINUTES) {
                return true;
            }
        }
        return false;
    }


    private void registerBroadcastReceiver() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DOWNLOAD_PROGRESS);
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(DOWNLOAD_PROGRESS)) {
                Log.d(TAG, "Downloaded file from endpoint successfully");
                readJSONFromFile();
            }
        }
    };

    private void downloadStoreData() {
        currentTime = Calendar.getInstance().getTimeInMillis();
        SharedPreferences.Editor sharedPrefEditor = mSharedPreferences.edit();
        sharedPrefEditor.putString("LastCallTime", String.valueOf(currentTime));
        sharedPrefEditor.apply();

        Intent intent = new Intent(this, DownloadService.class);
        startService(intent);
    }

    private void readJSONFromFile() {
        try {

            FileInputStream fis = new FileInputStream(new File(this.getFilesDir() + "/stores.json"));
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            Gson gson = new Gson();
            mStoreEntity = gson.fromJson(sb.toString(), StoresEntity.class);

            if (mStoreEntity != null) {
                Log.d(TAG, sb.toString());
                bindDataToAdapter(mStoreEntity);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void bindDataToAdapter(StoresEntity storesEntity) {
        if ((storesEntity != null) && (storesEntity.getStores().size() != 0)) {
            StoresAdapter storesAdapter = new StoresAdapter(storesEntity);
            storesAdapter.setOnItemClicked(this);
            rvStoresList.setAdapter(storesAdapter);
        }
    }

    @Override
    public void onItemClick(int position) {
        if (mStoreEntity != null) {
            Gson gson = new Gson();
            Intent intent = new Intent(this, StoreInfoActivity.class);
            intent.putExtra("JSON_DATA", gson.toJson(mStoreEntity.getStores().get(position)));
            startActivity(intent);
        }
    }

}
