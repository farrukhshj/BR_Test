package shuja.com.bottlerockettest.controller;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import shuja.com.bottlerockettest.R;
import shuja.com.bottlerockettest.model.Store;

public class StoreInfoActivity extends AppCompatActivity {

    public TextView tvName;
    public TextView tvAddress;
    public TextView tvCity;
    public TextView tvState;
    public TextView tvZipcode;
    public Button btnPhoneNumber;
    public Button btnGetDirection;

    public ImageView ivStoreLogoMain;
    public Gson gson;

    private Store mStoreObject;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_info);

        tvName = findViewById(R.id.tvName);
        tvAddress = findViewById(R.id.tvAddress);
        tvCity = findViewById(R.id.tvCity);
        tvState = findViewById(R.id.tvState);
        tvZipcode = findViewById(R.id.tvZipCode);
        btnPhoneNumber = findViewById(R.id.btnPhoneNumber);
        ivStoreLogoMain = findViewById(R.id.ivStoreLogoMain);
        btnGetDirection = findViewById(R.id.btnGetDirections);

        String storeDataInJson = getIntent().getStringExtra("JSON_DATA");
        if (storeDataInJson != null) {
            gson = new Gson();
            mStoreObject = gson.fromJson(storeDataInJson, Store.class);
            tvName.setText(mStoreObject.getName());
            tvAddress.setText(mStoreObject.getAddress() + ", ");
            tvCity.setText(mStoreObject.getCity());
            tvZipcode.setText(mStoreObject.getZipcode());
            btnPhoneNumber.setText(mStoreObject.getPhone());
            tvState.setText(mStoreObject.getState() + " - ");
            Picasso.get().load(mStoreObject.getStoreLogoURL()).into(ivStoreLogoMain);
        }

        btnGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String coordinates = mStoreObject.getLatitude() + "," + mStoreObject.getLongitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/dir/?api=1&destination=" + coordinates));
                startActivity(intent);
            }
        });

    }

    public void callStore(View v) {
        if(checkPermissions()){
            call();
        }
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call();
                } else {
                    Toast.makeText(this, "Goto phone settings and grant the required permissions to continue placing calls!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mStoreObject.getPhone()));
        startActivity(intent);
    }


}
