package shuja.com.bottlerockettest.controller;

public interface OnItemClicked {
    void onItemClick(int position);
}
